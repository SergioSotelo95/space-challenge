open class Rocket(var weight: Int, var maxWeight: Int, var cost: Int, var shield: Boolean = false) : SpaceShip {

    override fun launch(): Boolean {
        return true
    }

    override fun land(): Boolean {
        return true
    }

    override fun canCarry(Item: Item): Boolean {
        return weight + (Item.weight / 1000) <= maxWeight
    }

    override fun carry(Item: Item) {
        this.weight += (Item.weight / 1000)
    }

}