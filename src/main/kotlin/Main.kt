fun main(args: Array<String>) {
    val simulation = Simulation()

    fun u1(): Int {
        val arrP1 = simulation.loadItems("src/main/kotlin/Phase-1.txt")
        val arrP2 = simulation.loadItems("src/main/kotlin/Phase-2.txt")
        val phase1U1 = simulation.loadU1(arrP1)
        val phase2U1 = simulation.loadU1(arrP2)
        val totalP1U1: Int = simulation.runSimulation(phase1U1.setShields())
        val totalP2U1: Int = simulation.runSimulation(phase2U1.setShields())
        return totalP1U1 + totalP2U1
    }

    fun u2(): Int {
        val arrP1 = simulation.loadItems("src/main/kotlin/Phase-1.txt")
        val arrP2 = simulation.loadItems("src/main/kotlin/Phase-2.txt")
        val phase1U2 = simulation.loadU2(arrP1)
        val phase2U2 = simulation.loadU2(arrP2)
        val totalP1U2: Int = simulation.runSimulation(phase1U2.setShields())
        val totalP2U2: Int = simulation.runSimulation(phase2U2.setShields())
        return totalP1U2 + totalP2U2
    }

    val u1Budget = u1()
    val u2Budget = u2()

    println("Total budget Rocket U1: $u1Budget Million \nTotal budget Rocket U2: $u2Budget Million")
}

fun ArrayList<Rocket>.setShields(): ArrayList<Rocket> {
    this.forEachIndexed { index, element ->
        run {
                if ((index + 1) % 3 == 0){
                    element.shield = true
                }
            }

    }
    return this
}
