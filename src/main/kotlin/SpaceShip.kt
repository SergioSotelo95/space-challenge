interface SpaceShip {

    fun launch(): Boolean
    fun land(): Boolean
    fun canCarry(Item: Item): Boolean
    fun carry(Item: Item)
}