class U2() : Rocket(18, 29, 100) {
    override fun launch(): Boolean {
        val cargoLimit = 11
        var posOfExplosion: Int = (weight / cargoLimit) * 4
        if (this.shield){
            posOfExplosion = 0
            println("A U2 rocket with active shield has landed in the destination")
        }

        val oneHundred: IntRange = (1..100)
        return oneHundred.random() >= posOfExplosion
    }

    override fun land(): Boolean {
        val cargoLimit = 11
        var posOfCrash: Int = (weight / cargoLimit) * 8
        if (this.shield) posOfCrash = 0
        val oneHundred: IntRange = (1..100)
        return oneHundred.random() >= posOfCrash
    }
}

//