class U1() : Rocket(10, 18, 120) {
    override fun launch(): Boolean {
        //How this works: 1. We set the values of cargoLimit
        val cargoLimit = 8 //maxWeight(18) - rocket's weight(10) = cargoLimit(8)
        //2. We calculate the possibility of explosion and end up with a number lower than 100
        var posOfExplosion: Int = (weight / cargoLimit) * 5
        if (this.shield) {
            posOfExplosion = 0
            println("A U1 rocket with active shield has landed in the destination")
        }
        //NOTE: We have been told to include the rocket's weight into "cargoWeight",
        //since it is the only way this formula will not return 0
        //3. We create an array with numbers from 1 to 100
        val oneHundred: IntRange = (1..100)
        //4. If we pick a random number below 100 that is lower than
        //the number of possibilities, it returns true,
        //else, it returns false.
        return oneHundred.random() >= posOfExplosion
    }

    override fun land(): Boolean {
        val cargoLimit = 8
        var posOfCrash: Int = (weight / cargoLimit) * 1
        if (this.shield) posOfCrash = 0
        val oneHundred: IntRange = (1..100)
        return oneHundred.random() >= posOfCrash
    }
}

//