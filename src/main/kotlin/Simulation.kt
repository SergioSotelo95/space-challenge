import java.io.File

class Simulation {
    fun loadItems(fileName: String): ArrayList<Item> {
        val file = File(fileName)
        val arr = ArrayList<Item>()
        file.forEachLine {
            val keyVal = it.split("=")
            arr.add(Item(keyVal[0], keyVal[1].toInt()))
        }
        return arr
    }

    fun loadU1(itemArray: ArrayList<Item>): ArrayList<Rocket> {

        val rocketArray = ArrayList<Rocket>()
        fun itemsLeft(): Boolean {
            var flag = false
            for (item in itemArray) {
                if (item.weight != 0) flag = true
            }
            return flag
        }

        while (itemsLeft()) {
            val u1Rocket = U1()
            for (item in itemArray) {
                if (u1Rocket.canCarry(item) && item.weight != 0) {
                    u1Rocket.carry(item)
                    item.weight = 0
                }
            }
            rocketArray.add(u1Rocket)
        }
        return rocketArray
    }

    fun loadU2(itemArray: ArrayList<Item>): ArrayList<Rocket> {
        val rocketArray = ArrayList<Rocket>()

        fun itemsLeft(): Boolean {
            var flag = false
            for (item in itemArray) {
                if (item.weight != 0) flag = true
            }
            return flag
        }
        while (itemsLeft()) {
            val u2Rocket = U2()
            for (item in itemArray) {
                if (u2Rocket.canCarry(item) && item.weight != 0) {
                    u2Rocket.carry(item)
                    item.weight = 0
                }
            }
            rocketArray.add(u2Rocket)

        }
        return rocketArray
    }

    fun runSimulation(rocketArray: ArrayList<Rocket>): Int {
        var costSum = 0
        for (rocket1 in rocketArray) {

            when (rocket1.launch()) {
                true -> costSum += rocket1.cost
                false -> {
                    do {
                        costSum += rocket1.cost
                    } while (!rocket1.launch())
                }
            }


        }
        //We don't need to check if all rockets launched since after the loop all of them would have
        for (rocket2 in rocketArray) {
            //We only need to resend a rocket if it crashed, so true cases will do nothing

            when (rocket2.land()) {
                true -> continue
                false -> {
                    do {
                        do {
                            costSum += rocket2.cost
                            //If the rocket crashed, it will be resent, and only will
                            //check if landed after launch was successful
                        } while (!rocket2.launch())
                    } while (!rocket2.land())
                }

            }

        }
        return costSum
    }
}
